package com.shishuo.cms.service;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class ConfigServiceTest  extends BaseServiceTest{

	@Autowired
	private ConfigService configService;

	@Test
	public void testAddConfig() {
		assertEquals("h", configService.addConfig("h", "ew").getKey());
	}

	// @Test
	// public void testGetConfigPage() {
	// PageVo<Config> pageVo = configService.getConfigPage(1);
	// assertNotNull(pageVo);
	// assertEquals(6, pageVo.getList().size());
	// }

	@Test
	public void testDeleteConfigByKey() {
		assertEquals(1, configService.deleteConfigByKey("f"));
	}

	@Test
	public void testUpdagteConfigByKey() {
		assertEquals("a", configService.updagteConfigByKey("a", "ad").getKey());
	}

}
