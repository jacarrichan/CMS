package com.shishuo.cms.service;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.shishuo.cms.constant.LogConstant;
import com.shishuo.cms.entity.Log;
import com.shishuo.cms.util.JacksonUtil;

public class LogServiceTest  extends BaseServiceTest{

	@Autowired
	private LogService logService;

	 @Test
	 public void testAddLog() {
		 logService.addLog(LogConstant.Level.DEBUG ,"sdef");
	 //assertEquals("sdef", logService.addLog(LogConstant.Level.DEBUG ,"sdef").getDescription());
	 }

	@Test
	public void testGetLogPage() {
		assertEquals(2, logService.getLogPage(1).getList().size());
	}
	@Test
	public void testGetLogList() {
		List<Log>  page=logService.getLogList(0, 200);
		System.err.println(JacksonUtil.bean2Json(page));
	}

}
