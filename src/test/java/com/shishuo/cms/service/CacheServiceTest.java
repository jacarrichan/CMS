package com.shishuo.cms.service;

import java.util.Collection;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.support.SimpleCacheManager;

public class CacheServiceTest extends BaseServiceTest {
	@Autowired
	private SimpleCacheManager cacheManager;

	@Test
	public void getCache() {
		Collection<String> cacheNames = cacheManager.getCacheNames();
		for (String cacheName : cacheNames) {
			cacheManager.getCache(cacheName).clear();
		}
	}
}
