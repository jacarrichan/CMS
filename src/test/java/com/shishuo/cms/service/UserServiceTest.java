package com.shishuo.cms.service;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class UserServiceTest  extends BaseServiceTest {

	@Autowired
	private UserService userService;

	// @Test
	// public void testAddUser() {
	// assertEquals(1, userService.addUser(6, 32, 113, "sdg").getOpenId());
	// ;
	// }

	@Test
	public void testGetUserById() {
		assertEquals(2, userService.getUserById(2).getUserId());
	}

	@Test
	public void testGetUserPage() {
		assertEquals(4, userService.getUserPage(1).getList().size());
	}

	@Test
	public void testDeleteUserById() {
		assertEquals(1, userService.deleteUserById(2));
	}

}
