package com.shishuo.cms.service;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.shishuo.cms.entity.vo.ArticleVo;
import com.shishuo.cms.entity.vo.PageVo;
import com.shishuo.cms.exception.FolderNotFoundException;

public class ArticleServiceTest  extends  BaseServiceTest {
	@Autowired
	private ArticleService articleService;
	
	@Test
	public void testGetArticlePageByFolderId() throws FolderNotFoundException{
		PageVo<ArticleVo> articleVoList = articleService.getArticlePageByFolderId(4, 1, 3);
		for (ArticleVo articleVo : articleVoList.getList()) {
			System.out.println(articleVo.getTitle());
		}
	}

}
