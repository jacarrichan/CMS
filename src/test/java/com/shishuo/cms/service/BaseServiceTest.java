package com.shishuo.cms.service;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

@ContextConfiguration(locations = { "/applicationContext.xml" })
public abstract class BaseServiceTest extends AbstractJUnit4SpringContextTests {

}
